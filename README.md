# 审计军火库 Audit Guid
![工具](https://img.shields.io/badge/工具-tools-red.svg) ![法规](https://img.shields.io/badge/法规-refrence-orange.svg) ![网课](https://img.shields.io/badge/网课-learn-yellow.svg) ![书籍](https://img.shields.io/badge/书籍-books-green.svg) ![媒体](https://img.shields.io/badge/媒体-media-blue.svg) ![分享](https://img.shields.io/badge/众人拾柴火焰高-share-pink.svg) 


本`repo`用来收集公开的一些审计资源、工具、视频、资料。

希望这个平台能给大家提供一个方便，同时它也需要大家能够分享，让它不断的丰富。

审计行业本是一个十分封闭的行业，缺少交流、沟通、分享。但我希望它能变得好一点，对于新入行的人有途径获取一些好的学习资源。

如果你也是这样认为，可以贡献你的一份力。

只需要点击 [Issues](https://gitee.com/nigo81/audit-guid/issues/new?issue%5Bassignee_id%5D=0&issue%5Bmilestone_id%5D=0)，新建一个issue，然后把你觉得好的资源的名称、链接以及相关描述写出来就完成了。(注：单个文件建议使用[蓝奏云](https://www.lanzou.com/)没有提取码，也没有速度限制)

当然，你如果会使用`git`，你也可以提交`pull requests`。

***

**目录**


<!-- vim-markdown-toc GFM -->

* [工具](#工具)
    * [财务审计工具](#财务审计工具)
        * [财务数据工具](#财务数据工具)
    * [IT审计工具](#it审计工具)
    * [办公效率工具](#办公效率工具)
* [实用网站](#实用网站)
    * [综合数据查询平台](#综合数据查询平台)
    * [工商信息查询](#工商信息查询)
    * [IPO及上市公司定期公告查询](#ipo及上市公司定期公告查询)
    * [审计程序检索](#审计程序检索)
        * [汇率查询](#汇率查询)
        * [利率查询](#利率查询)
        * [海关信息查询](#海关信息查询)
        * [商标专利](#商标专利)
        * [动产抵押查询](#动产抵押查询)
        * [国家科技成果库登记结果查询](#国家科技成果库登记结果查询)
        * [征信报告查询](#征信报告查询)
        * [海外银行函证](#海外银行函证)
        * [诉讼](#诉讼)
    * [事务所信息查询](#事务所信息查询)
    * [税收法规查询](#税收法规查询)
    * [处罚信息](#处罚信息)
* [法规学习](#法规学习)
    * [准则](#准则)
    * [其它](#其它)
* [网课](#网课)
* [书籍](#书籍)
* [其它](#其它-1)

<!-- vim-markdown-toc -->


## 工具

### 财务审计工具

- [审计工具箱](https://gitee.com/wwwwwc/audbox) 快递截图、工商截图、询证函处理、地址复核、海关查询、公告下载、地址补全集合成一个exe文件。
- [tools for auditor](https://github.com/nigo81/tools-for-auditor) Excel VBA编写的一系列工具
- [万能函证生成器](https://gitee.com/nigo81/audittool) - [使用方法](https://www.bilibili.com/video/BV1eK411u7cz)  采用根据不同Word自动生成Excel模版，只需要在生成的Excel模版中填写数据，点点按钮就能轻松生成
- [方方格子](http://www.ffcell.com/) -Excel插件，包含一系列Excel实用功能

#### 财务数据工具
- [同行业财务指标查询工具](https://pan.baidu.com/s/1qtkLv475LvqGcOHKuK1uxw)  -(密码: gkp5)Excel VBA写的宏获取新浪财经指标数据包含在[tools for auditor](https://github.com/nigo81/tools-for-auditor)中。
- [国泰君安数据库](https://www.gtarsc.com/) -查询上市公司财务数据（付费）， [使用方法](https://mp.weixin.qq.com/s/_d42SKSUyWBKoM1JRNbQMA) 。

### IT审计工具

- [mysql](https://www.mysql.com/downloads/) -免费数据库，可以对大数据量数据进行处理分析，需要掌握SQL语言。
- [SQL基础语法](https://www.w3school.com.cn/sql/index.asp) -快速掌握SQL基本语法。 
- [SQL练习网站](https://sqlzoo.net/) -通过练习掌握SQL用法。
- [python教程](https://www.liaoxuefeng.com/wiki/1016959663602400) -免费python教程。
- [power bi](https://powerbi.microsoft.com/zh-cn/downloads/) -微软出的免费的BI工具。
- [superset](https://superset.apache.org/) -免费的可视化数据分析工具(BI工具)，基于python。
- [gephi](https://gephi.org/) -免费的网络关系分析工具。
- [plotly](https://plotly.com/python/) -免费的数据可视化库(python库)。
- [地址解析为省市区](https://github.com/DQinYuan/chinese_province_city_area_mapper) -一个用于提取简体中文字符串中省，市和区并能够进行映射，检验和简单绘图的python模块。 
- [IP地址解析](https://gitee.com/lionsoul/ip2region) -ip2region  准确率99.9%的离线IP地址定位库。
- [IP地址查询网站](https://www.ipip.net/ip.html) -IPIP网站查询IP地址，解析出其物理地址。
- [手机号码归属地解析](https://github.com/ls0f/phone) -手机号码归属地库。
- [tushare金融数据](https://tushare.pro/) -金融数据接口（数据获取需要积分）
- [行政区划数据库](https://gitee.com/xusimin/area) -全国行政区划，省市区镇四级，包含名称、完整名称、经纬度、区号、邮编、行政区划代码、拼音。


### 办公效率工具

- [total commander](https://www.ghisler.com/) -强大的资源管理器，可以提高操作文件的效率。(免费的够用)
- [utools](https://u.tools/) -非常强大的一个应用启动器，同时也是生产力工具集，集成了很多有用的小插件。(免费)
- [Quicker](https://getquicker.net/) -办公效率工具，可以为任何常用内容建立捷径，也可以自己组合动作去完成某个特定的功能。不善于创造动作的同学可以使用他人分享的动作。


## 实用网站

### 综合数据查询平台

- [青藤数据](http://www.qingtengdata.com/) -审计相关数据查询网站。实务问答、税务问答、关键审计事项、公告查询、财务法规、税务法规。(免费)
- [见微数据](https://www.jianweidata.com/) -公告查询、IPO反馈、问询。一个月试用期。(付费)
- [荣大二郞神](http://doc.rongdasoft.com/) -公告、反馈，与见微相似。(付费)
- [会计视野论坛](https://bbs.esnai.com/forum-7-1.html) -审计行业最专业的实务问题交流平台。

### 工商信息查询

- [天眼查](https://www.tianyancha.com/) 
- [信用中国](https://www.creditchina.gov.cn) 
- [深圳商事主体查询](https://amr.sz.gov.cn/outer/entSelect/gs.html) 
- [国外公司的工商信息](registers) 
- [香港公司的工商信息](https://www.icris.cr.gov.hk/csci/) 
- [国家企业信用信息公示系统](http://www.gsxt.gov.cn/index.html) 
- [国家市场监督管理总局](http://zwfw.samr.gov.cn/wyc/) 
- [小微企业名录](http://xwqy.gsxt.gov.cn) 

### IPO及上市公司定期公告查询

- [巨潮资讯网](http://www.cninfo.com.cn/new/index) 
- [中国证券监督管理委员会（信息披露平台）](http://eid.csrc.gov.cn/) 
- [中国证券监督管理委员会](http://www.csrc.gov.cn/pub/newsite/) 
- [股转系统公告查询](http://www.neeq.com.cn/disclosure/overview_information.html) 
- [中小板、主板的排队名单](http://www.csrc.gov.cn/pub/newsite/zxgx/jigbsdt/) 
- [科创板名单及反馈查询](http://kcb.sse.com.cn/renewal/) 
- [创业板名单及反馈查询](http://listing.szse.cn/projectdynamic/ipo/index.html) 
- [精选层名单及反馈查询](http://www.neeq.com.cn/zone/audit/project_news.html) 

### 审计程序检索

- [银行卡号归属地查询](http://www.guabu.com/bank/) 

#### 汇率查询
- [国家外汇管理局](http://www.safe.gov.cn/safe/rmbhlzjj/index.html) 
- [中国人民银行](http://www.pbc.gov.cn/rmyh/108976/index.html) 
- [中国货币网](http://www.chinamoney.com.cn/) -有汇率、利率等相关信息，由中国外汇交易中心主办。

#### 利率查询
- [LIBOR历史利率](http://data.bank.hexun.com/yhcj/cj.aspx?r=0000000001000000&t=12&page=24) 
- [贷款基准利率查询](http://www.pbc.gov.cn/zhengcehuobisi/125207/125213/125440/125838/125888/index.html) 
- [中国债券信息网-中债收益率](http://yield.chinabond.com.cn/cbweb-mn/yield_main?locale=zh_CN) 
- [贷款利率和LPR](http://www.pbc.gov.cn/zhengcehuobisi/125207/125213/125440/index.html) 
- [银行拆借利率](http://data.bank.hexun.com/yhcj/cj.aspx) 
#### 海关信息查询
- [海关总署政府服务窗口](http://customs.gjzwfw.gov.cn) 
- [通关流转状态](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/tglzztPC/index.html) 
- [舱单通关状态查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/cdtgztPC/index.html) 
- [进出口商品税率查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/jckspslPC/index.html) 
- [本国子目查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/bengzmPC/index.html) 
- [税则商品品目注释](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/szsmzsPC/index.html) 
- [归类决定裁定](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/gljdcdPC/index.html) 
- [重点商品查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/zhongdspcxPC/index.html) 
- [税目税号查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/smshPC/index.html) 
- [舱单信息](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/cdxxPC/index.html) 
- [企业信息公示](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/qyxigsPC/index.html) 
#### 商标专利
- [国家知识产权](https://www.cnipa.gov.cn/col/col1510/index.html) 
- [专利检索及分析系统](http://pss-system.cnipa.gov.cn/sipopublicsearch/portal/uiIndex.shtml) 
- [中国及多国专利审查信息查询](http://cpquery.cnipa.gov.cn) 
- [中国专利公布公告](http://epub.cnipa.gov.cn) 
- [商标查询](http://sbj.cnipa.gov.cn/sbcx/) 
- [商标注册审查决定文书](http://wssq.sbj.cnipa.gov.cn:9080/tmsve/zccw_getMain.xhtml) 
- [商标异议决定文书](http://wssq.sbj.cnipa.gov.cn:9080/tmsve/yycw_getMain.xhtml) 
- [商标评审裁定/决定文书](http://wssq.sbj.cnipa.gov.cn:9080/tmsve/pingshen_getMain.xhtml) 
- [商标注册证明公示](http://wsgs.sbj.cnipa.gov.cn:9080/tmpu/) 
- [商标公告](http://wsgg.sbj.cnipa.gov.cn:9080/tmann/annInfoView/homePage.html) 
#### 动产抵押查询
- [全国市场监管动产抵押登记业务系统](http://dcdy.gsxt.gov.cn/loginSydq/index.xhtml?isNotLogin=1) 
#### 国家科技成果库登记结果查询
- [国家科技成果库登记结果查询](https://www.tech110.net/portal.php?mod=list&catid=538) 
#### 征信报告查询
- [中国人民银行征信中心](http://www.pbccrc.org.cn/) 
#### 海外银行函证
- [comformation](https://zs.confirmation.com/) 
#### 诉讼
- [全国法院被执行人信息查询](http://zhixing.court.gov.cn/search/) 
- [中国裁判文书网](http://wenshu.court.gov.cn/) 
- [中国执行信息公开网](http://zxgk.court.gov.cn/) 

### 事务所信息查询

- [财政部政务服务窗口](http://mof.gjzwfw.gov.cn) 
- [证券资格事务所信息查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/zqsxxcx/index.html) 
- [事务所信息查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/swsxxcx/index.html) 
- [注册会计师信息查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/zckjscx/index.html) 
- [事务所重名查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/swscmcx/index.html) 
- [代理记账机构查询](http://app.gjzwfw.gov.cn/jmopen/webapp/html5/dljzjgcx/index.html) 

### 税收法规查询
- [国家税务总局](http://www.chinatax.gov.cn/chinatax/n810346/index.html) 
- [税收政策库](http://www.chinatax.gov.cn/chinatax/n810341/n810825/index.html?title=) 
- [国家税务总局全国增值税发票查验平台](https://inv-veri.chinatax.gov.cn) 
- [信用A级纳税人查询](http://hd.chinatax.gov.cn/nszx/InitCredit.html) 
- [重大税收违法失信案件信息查询](http://www.chinatax.gov.cn/chinatax/c101249/n2020011502/index.html) 
- [涉税专业服务机构查询](https://12366.chinatax.gov.cn/sszyfw/bulletinBoard/main) 
- [出口退税率查询](http://www.chinatax.gov.cn/n810346/index.html) 
- [税收条约](http://www.chinatax.gov.cn/chinatax/n810341/n810770/index.html) 
- [税务公报](http://www.chinatax.gov.cn/chinatax/n810341/n810765/index.html) 

### 处罚信息
- [上市公司违法违规信息](https://www.investor.org.cn/was5/web/search?channelid=223359) 
- [挂牌公司违法违规信息](https://www.investor.org.cn/was5/web/search?channelid=223359) 
- [中介机构违法违规信息](https://www.investor.org.cn/was5/web/search?channelid=223359) 
- [中国市场监督行政处罚文书网](http://cfws.samr.gov.cn) 



## 法规学习

### 准则
- [财政部-企业会计准则](http://kjs.mof.gov.cn/zt/kjzzss/kuaijizhunzeshishi/) 
- [财政部-企业会计准则解释](http://kjs.mof.gov.cn/zt/kjzzss/qykjzzjs/) 
- [财政部-发布收入准则、租赁准则、股份支付准则应用案例](http://kjs.mof.gov.cn/zt/kjzzss/srzzzq/) 
- [财政部-收入准则实施问答](http://kjs.mof.gov.cn/zt/kjzzss/sswd/srzzsswd/) 
- [财政部-借款费用准则实施问答](http://kjs.mof.gov.cn/zt/kjzzss/sswd/jkwd/) 
- [财政部-其他相关实施问答](http://www.mof.gov.cn/gongzhongcanyu/zixunfankui/) 
- [企业会计准则及应用指南2021年5月修订-下载](https://wwe.lanzoui.com/ib3yQropyid)
- [中国注册会计师审计准则2019-下载](https://pan.baidu.com/s/18ugaMenieBHB8BNArGVPKQ) -（提取码：b9tn）-2019年发布的18项中国注册会计师审计准则+2020发布的5项中国注册会计师审计准则问题解答
- [中国注册会计师审计准则应用指南-下载](https://www.lanzoui.com/i7um6ch) 

### 其它
- [监管规则适用指引——会计类第1号](http://www.csrc.gov.cn/newsite/ztzl/jggzsyzy/202011/t20201113_386201.html) 
- [中国证监会发行审核业务问答](http://www.csrc.gov.cn/pub/newsite/fxjgb/fxbzcfg/fxbfxjgwd/202006/t20200610_377997.html) 
- [中国证监会监管规则](http://www.csrc.gov.cn/pub/newsite/kjb/kjbzcgf/xsjzj/sjpgjggz/) 
- [北京注册会计师协会专业技术委员会 专家提示](http://www.bicpa.org.cn/zyfwz/zyfw/index.html) 
- [德勤发布会计研究](http://www.casplus.com/home.asp) 

## 网课

- [瑞华会计师事务所合伙人手把手教你审计实操](https://www.bilibili.com/video/BV1Ts411L7mz?from=search&seid=4193060424620229640) B站上提供的审计实操课程。
- [审计效率提升课](https://space.bilibili.com/229695603/channel/detail?cid=86210) B站上讲解用excel常用函数以及VBA工具提高效率。
- [上海证券交易所公开课](http://training.sseinfo.com/sse/index.do) 

## 书籍

- [让数字说话](https://book.douban.com/subject/26746460/) -作者以浅显易懂的语言、幽默诙谐的比喻和旁征博引的故事引领我们开启一段奇妙的阅读旅程。
- [会计准则内在逻辑](https://book.douban.com/subject/26871620/) -理解会计准则原理的不错的书。
- [审计excel技能手册](https://wwe.lanzoui.com/iB6l3rpmbaf) -详细讲解了审计工作中需要用到的Excel基本技能，比较推荐。
- [审计效率手册](https://book.douban.com/subject/34886553/) -讲解了Excel函数和Excel VBA制作一些审计工具。
- [瑞华研究2010～2019汇编](https://bbs.esnai.com/thread-5413973-1-1.html) -审计实务问题汇编
- [中审众环研究](https://wwe.lanzoui.com/iBRuSrpljuh) 
- [上市公司执行企业会计准则案例解析（2020）](https://wwe.lanzoui.com/i3PgOrpljob) 
- [IPO审核问答汇编20210716](https://wwe.lanzoui.com/i0tYwrpljna) 
- [行政事业单位审计常见问题200案例](https://www.aliyundrive.com/s/bSk7pyuSdqQ) -(提取码：gkt0)

## 其它

- [各会计科目审计经验总结](https://wwe.lanzoui.com/idPyVi8jtfe) -林铖总结的每个科目关注点、需要资料、提高效率方法。
- [会计视野论坛陈版主答疑汇总](https://wwe.lanzoui.com/b01nyhiod) -(密码:1mki)论坛CPA业务探讨版块问答的汇总。
- [合并财务报表理论与实务--复杂股权结构与成本法合并](https://wwe.lanzoui.com/ib5c2ronong) -及晓慧老师分享的合并报表课件。
